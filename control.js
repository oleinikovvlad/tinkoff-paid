 
var pop_up = document.querySelector('.pop-up');
var count = document.querySelector('.count');

document.querySelector('.basket').addEventListener("click", function(){
    construct_form();
    document.querySelectorAll('.quantity').forEach(element => {
        element.oninput = function() {
            change_product(this.id,this.value);
        };
    });
    document.getElementById('button').addEventListener("click", getPaid);
    pop_up.classList.add("active");
    
});
document.querySelector('.close_form').addEventListener("click", function(){
    pop_up.classList.remove("active");
});
document.querySelectorAll('.add-product').forEach(element => {
    element.addEventListener("click", add_product);
});

var Product = {};

function add_product(){
    let name = document.querySelector('#'+this.id+'_name').value;
    let price = document.querySelector('#'+this.id+'_price').value;
    if (Product.hasOwnProperty(this.id)){
        Product[this.id].quantity = Product[this.id].quantity*1+1;
    } else {
        Product[this.id] = {
            'name' : name,
            'price': price,
            'quantity': 1
        }

    }
    var basket_count = 0;
    for (var id in Product) {
        basket_count = basket_count + Product[id].quantity*1;
    }
    if (!count.classList.contains('active')){
        count.classList.add("active");
    }
    count.innerHTML = basket_count;
    
}

function construct_form(){
    if (Object.keys( Product ).length){
        let form = document.createElement("div");
        form.className = "form";
        let product_price = 0;
        for (var id in Product) {
            let div = document.createElement('div');
            div.className = "product";
            div.id = id+"_product";

            let name = document.createElement('span');
            name.className = "name";
            name.innerHTML = Product[id].name;
            div.append(name);

            let input = document.createElement('INPUT');
            input.setAttribute('type' , 'number');
            input.setAttribute('min' , '0');
            input.value = Product[id].quantity;
            input.id = id+"_quantity";
            input.className = "quantity";
            div.append(input);

            let price = document.createElement('span');
            price.className = "price";
            price.id = id+"_quantity_price";
            price.innerHTML = Product[id].price*Product[id].quantity+" р.";
            div.append(price);

            form.append(div);

            product_price = Product[id].price*Product[id].quantity+product_price;
        }
        let button = document.createElement('div');
        button.className='buttons';
        button.id = 'button'
        button.innerHTML = 'Купить за: '+product_price+' р.';
        form.append(button);

        document.querySelector('.form').replaceWith(form);


    } else {
        create_message();
    }
}

function change_product(id,value){
    id = id.replace('_quantity','')
    if (value==0){
         
        delete Product[id];
        document.getElementById(id+'_product').remove();
        if (Object.keys( Product ).length){
            create_price();
        } else {
            create_message();
        }
    }else{
        
        Product[id].quantity = value;
        document.getElementById(id+'_quantity_price').innerText = value* Product[id].price+' р.';
        create_price();
    }
}

function create_message(){
    let span = document.createElement('span');
    span.className = "message";
    span.innerHTML = "Нет добавленных товаров";
    let div = document.createElement("div");
    div.className = "form";
    div.append(span);
    
    document.querySelector('.form').replaceWith(div);
    count.classList.remove("active");
}
function create_price(){
    var price =  0;    
    var basket_count = 0;
    for (var id in Product) {
        basket_count = basket_count + Product[id].quantity*1;
        price = Product[id].price*Product[id].quantity+price;
    }
    count.innerHTML = basket_count;
    document.getElementById('button').innerHTML = 'Купить за: '+price+' р.';
}

function getPaid() {
    let List = new Array();
    var price =  0;    
    for (var id in Product) {
        price_obj=Product[id].price*Product[id].quantity;
        obj={
            "Name": Product[id].name,
            "Price": Product[id].price*100,
            "Quantity": Product[id].quantity,
            "Amount": price_obj*100,
            "Tax": "vat10"
        }
        List.push(obj)
        price = price_obj+price;
    }
    price= price*100;
    

    var data = {
        TerminalKey : "TinkoffBankTest",
        Amount : price,
        OrderId: "21050",
        Description: "Тестовое задание от Tinkoff",
        Receipt: {
            Email: "a@test.ru",
            Phone: "+79999999999",
            EmailCompany: "b@test.ru",
            Taxation: "osn",
            Items: List,
        }
    };
    console.log(data);
    
    $.ajax({
        url: 'https://securepay.tinkoff.ru/v2/Init',
        type: "POST",
        contentType: "application/json",
        data: data,
        success: function (response) {
            location=response.PaymentURL;
        },
        error: function (response) {
            alert(response.statusText)
        }
    });    
}